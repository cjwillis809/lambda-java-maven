package validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.GetItemRequest;

public class Test {
	
	public static void main(String[] args) {
		
		final String tableName = "UrlRanks";
		final List<Map<String,String>> outputList = new ArrayList<>();

		// puts each url into a map and adds it the the output list
		//for(String url : this.urlList) {
		    final Map<String,String> outputItem1 = new HashMap<>();
		    outputItem1.put("UrlName", "wsp.com");
		    outputList.add(outputItem1);
		//}
		
		// Connect to the db and run the query for each url
		       BasicAWSCredentials basic = new BasicAWSCredentials("AKIAINLTALKWM6B2TLEA", "3dusQmRj9LJDXWoM/IgQdiZIf11XlSkR1Y8bsmRy"); 
				AmazonDynamoDBClient ddb = new AmazonDynamoDBClient(basic);
				ddb.setRegion(Region.getRegion(Regions.US_WEST_2));
				DynamoDB dynamodb = new DynamoDB(ddb);
				for(Map<String, String> outputItem : outputList) {
					final String url = outputItem.get("UrlName");
					final Map<String, AttributeValue> keyValue = new HashMap<>();
					keyValue.put("UrlName", new AttributeValue(url));
		    		final GetItemRequest request = new GetItemRequest()
						                       .withKey(keyValue)
						                       .withTableName(tableName);
		    		try {
			    		final Map<String,AttributeValue> returned_item = ddb.getItem(request).getItem();
			    		// if url is in the db, get the rank
		    			if (returned_item != null) {
			    			Set<String> keys = returned_item.keySet();
				    		for (String key : keys) {
					    		if(key.equals("UrlRank")) {
					    			System.out.println("Rank in database is " + returned_item.get(key).getN());
					    			outputItem.put("rank", returned_item.get(key).getN());
							    }
						    }
				    	// if url is not in the database, give it a rank of 50 and ad it.
		    			} else {
		    				System.out.println("Not found from database");
			    			final int rank = 50;
				    		final Table table = dynamodb.getTable(tableName);
					    	final Item item = new Item().withPrimaryKey("UrlName", url).withNumber("UrlRank", rank);
						    PutItemOutcome outcome = table.putItem(item);
			    			outputItem.put("rank", Integer.toString(rank));
		    			}
			    	} catch (AmazonServiceException e) {
				    	// database read failed for some reason
					   e.printStackTrace();
				    }

				}
	}

}

