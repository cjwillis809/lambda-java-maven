package validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

/**
 * Calculates a rank for the validity of a list of urls
 * 
 * @author Chris
 *
 */
public class NewsValidator {
	/**
	 * Runs a list of urls through the ranking algorithms
	 * @param input map of lists corresponds to a http get request parameter with a list of urls.  (i.e. requestUrl=["abc.com","facebook.com"])
	 * @param context the lambda context object required to run as an AWS lambda
	 * @return a list of ranks in the same order as the list of urls
	 */
	public List<String> myHandler(Map<String, List<String>> input, Context context) {
		final LambdaLogger logger = context.getLogger();
		final List<String> urlList = new ArrayList<>();
		// construct a URLChecker with the urls in the "requestUrl" get parameter
        final UrlChecker urlCheck = new UrlChecker(input.get("requestUrls"));
		
        List<Map<String,String>> rankList = null;
        String error = "";
        try {
        	// Get a list of ranks from the URL Checkker
        	rankList = urlCheck.getRank();
        	// add each rank to the output list
        	for(Map<String,String> rankMap : rankList) {
                urlList.add(rankMap.get("rank"));
        	}
        } catch(RankCalculationFailureException e) {
        	error = "url check failed: " + e.getMessage();
        	logger.log(error);
        	// add error message to url list...  doesn't work yet...
            urlList.add(e.getMessage());
        }
 
		return urlList;
	}
	
	public int voteHandler(Map<String, List<String>> input, Context context) {
		final LambdaLogger logger = context.getLogger();
		int newRank = -1;
		final List<String> urlList = new ArrayList<>();
		// construct a URLChecker with the urls in the "requestUrl" get parameter
        //final UrlChecker urlCheck = new UrlChecker(input.get("requestUrls"));
		String url = input.get("url").get(0);
		String vote = input.get("vote").get(0);
        List<Map<String,String>> rankList = null;
        String error = "";
        try {
        	// Get a list of ranks from the URL Checkker
        	VotingIncrementer voteincrement = new VotingIncrementer(url, vote);
        	newRank = voteincrement.dovoting();
        } catch(Exception e) {
        	error = "url check failed: " + e.getMessage();
        	logger.log(error);
        	// add error message to url list...  doesn't work yet...
        } 
		return newRank;
	}
}
