package validator;

import org.junit.Assert;

import org.junit.Test;

public class RequestClassTest {

	@Test
	public void requestClassConstruction() {
		RequestClass requestClass = new RequestClass("asdf.com");
		Assert.assertEquals("asdf.com", requestClass.getRequestUrl());
	}

}
